package exceptions;

public class InvalidBooleanOperator extends Exception{
	public InvalidBooleanOperator() {
		super("Invalid boolean operator");
	}
}
