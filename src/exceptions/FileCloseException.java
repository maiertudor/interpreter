package exceptions;

public class FileCloseException extends Exception{

	public FileCloseException() {
		super("No such file found in FileTable");
	}
}
