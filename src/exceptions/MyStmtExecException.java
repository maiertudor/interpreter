package exceptions;

public class MyStmtExecException extends Exception {

	public MyStmtExecException() {
		super("Finished execution stack");
	}

	@Override
	public String getMessage() {
		return super.getMessage();
	}
}
