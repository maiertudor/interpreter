package exceptions;

public class FileOpenedException extends Exception {

	public FileOpenedException() {
		super("File already opened!");
	}

	@Override
	public String getMessage() {
		return super.getMessage();
	}
}
