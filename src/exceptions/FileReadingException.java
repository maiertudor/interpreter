package exceptions;

public class FileReadingException extends Exception {
	public FileReadingException() {
		super("No such file found in FileTable!");
	}
}
