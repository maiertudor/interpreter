package repository;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import model.PrgState;

public class MyRepository implements IRepository, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<PrgState> repoList;
	private PrgState currentPrg;
	private BufferedWriter fileWriter;
	private int stateNr;
	private String filePath;
	private ObjectOutputStream oos;
	private boolean closed = false;
	private ObjectOutputStream oosAppend;

	public MyRepository(List<PrgState> repoList) {
		super();
		this.repoList = repoList;
	}

	public MyRepository(PrgState currentPrg, String filePath) throws IOException {
		super();
		this.currentPrg = currentPrg;
		this.filePath = filePath;
		fileWriter = new BufferedWriter(new FileWriter(filePath));
		FileOutputStream fout = new FileOutputStream(filePath.replace("txt", "ser"));
		oos = new ObjectOutputStream(fout);
		// fileWriter = new FileWriter(file);
		this.repoList = new ArrayList<PrgState>();
		this.repoList.add(currentPrg);
		stateNr = 1;
	}

	public List<PrgState> getPrgList() {
		return repoList;
	}

	public void setPrgList(List<PrgState> repoList) {
		this.repoList = repoList;
	}

	public void logPrgStateExec(PrgState prgState) throws IOException {
		String stateEnded = "\n================================S" + stateNr + "================================\n\n";
		fileWriter.append(stateEnded);
		fileWriter.append(prgState.toStr());
		stateNr++;
	}

	public void closeFile() throws IOException {
		fileWriter.close();
	}

	@Override
	public void serializeFile(PrgState prg) throws IOException {
		oos.writeObject(prg);
		oos.close();
	}

	@Override
	public void deserializeFile() throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream in = null;
		in = new ObjectInputStream(new FileInputStream(filePath.replace("txt", "ser")));

		PrgState crtPrg = (PrgState) in.readObject();
		System.out.println(crtPrg.toStr());
		
		in.close();
	}

}
