package repository;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import model.PrgState;

public interface IRepository {
	
	public List<PrgState> getPrgList();
	
	public void setPrgList(List<PrgState> repoList);

	public void logPrgStateExec(PrgState prg) throws IOException;

	public void closeFile() throws IOException;
	
	public void serializeFile(PrgState prg) throws IOException;
	
	public void deserializeFile() throws FileNotFoundException, IOException, ClassNotFoundException;
}
