package view;

import java.io.IOException;

import controller.Controller;
import model.PrgState;
import model.ADT.FileTable;
import model.ADT.MyDictionary;
import model.ADT.MyHeap;
import model.ADT.MyList;
import model.ADT.MyStack;
import model.expressions.ArithExp;
import model.expressions.ConstExp;
import model.expressions.ReadHeapExp;
import model.expressions.VarExp;
import model.statements.AssignStmt;
import model.statements.CloseFile;
import model.statements.CompStmt;
import model.statements.ForkStmt;
import model.statements.HeapAllocStmt;
import model.statements.IStmt;
import model.statements.IfStmt;
import model.statements.OpenFile;
import model.statements.PrintStmt;
import model.statements.ReadFile;
import model.statements.WhileStmt;
import model.statements.WriteHeap;
import repository.IRepository;
import repository.MyRepository;

public class Interpreter {

	public static void main(String[] args) {

		/*
		 * First Statement Example
		 */
		IStmt ex1 = new CompStmt(new AssignStmt("v", new ConstExp(2)), new PrintStmt(new VarExp("v")));
		PrgState prg1 = new PrgState(new MyStack<>(), new MyDictionary<>(), new MyList<>(), ex1);
		Controller ctrl1 = null;
		try {
			IRepository repo1 = new MyRepository(prg1, ".//logs//log1.txt");
			ctrl1 = new Controller(repo1);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		/*
		 * Second Statement Example
		 */
		IStmt ex2 = new CompStmt(
				new AssignStmt("a",
						new ArithExp("+", new ConstExp(2), new ArithExp("*", new ConstExp(3), new ConstExp(5)))),
				new CompStmt(new AssignStmt("b", new ArithExp("+", new VarExp("a"), new ConstExp(1))),
						new PrintStmt(new VarExp("b"))));
		PrgState prg2 = new PrgState(new MyStack<>(), new MyDictionary<>(), new MyList<>(), ex2);
		Controller ctrl2 = null;
		try {
			IRepository repo2 = new MyRepository(prg2, ".//logs//log2.txt");
			ctrl2 = new Controller(repo2);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		/*
		 * Third example
		 */
		IStmt ex3 = new CompStmt(new AssignStmt("a", new ArithExp("-", new ConstExp(2), new ConstExp(2))), new CompStmt(
				new IfStmt(new VarExp("a"), new AssignStmt("v", new ConstExp(2)), new AssignStmt("v", new ConstExp(3))),
				new PrintStmt(new VarExp("v"))));
		PrgState prg3 = new PrgState(new MyStack<>(), new MyDictionary<>(), new MyList<>(), ex3);
		Controller ctrl3 = null;
		try {
			IRepository repo3 = new MyRepository(prg3, ".//logs//log3.txt");
			ctrl3 = new Controller(repo3);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		/*
		 * Fourth Statement example using file operations
		 */
		IStmt ex4 = new CompStmt(new OpenFile("var_f", "test.in"),
				new CompStmt(new ReadFile(new VarExp("var_f"), "var_c"),
						new CompStmt(
								new IfStmt(new VarExp("var_f"),
										new CompStmt(new ReadFile(new VarExp("var_f"), "var_c"),
												new PrintStmt(new VarExp("var_c"))),
										new PrintStmt(new ConstExp(0))),
								new CloseFile(new VarExp("var_f")))));
		PrgState prg4 = new PrgState(new MyStack<>(), new MyDictionary<>(), new MyList<>(), new FileTable<>());
		prg4.getExeStack().push(ex4);
		Controller ctrl4 = null;
		try {
			IRepository repo4 = new MyRepository(prg4, ".//logs//log4.txt");
			ctrl4 = new Controller(repo4);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		IStmt ex5 = new CompStmt(new AssignStmt("v", new ConstExp(10)),
				new CompStmt(new HeapAllocStmt("v", new ConstExp(20)),
						new CompStmt(new HeapAllocStmt("a", new ConstExp(22)), new PrintStmt(new VarExp("v")))));

		PrgState prg5 = new PrgState(new MyStack<>(), new MyDictionary<>(), new MyList<>(), ex5, new MyHeap<>());
		Controller ctrl5 = null;
		try {
			IRepository repo5 = new MyRepository(prg5, ".//logs//log5.txt");
			ctrl5 = new Controller(repo5);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		IStmt ex6 = new CompStmt(new AssignStmt("v", new ConstExp(10)),
				new CompStmt(new HeapAllocStmt("v", new ConstExp(20)),
						new CompStmt(new HeapAllocStmt("a", new ConstExp(22)), new PrintStmt(new ReadHeapExp("v")))));

		PrgState prg6 = new PrgState(new MyStack<>(), new MyDictionary<>(), new MyList<>(), ex6, new MyHeap<>());
		Controller ctrl6 = null;
		try {
			IRepository repo6 = new MyRepository(prg6, ".//logs//log6.txt");
			ctrl6 = new Controller(repo6);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		IStmt ex7 = new CompStmt(new AssignStmt("v", new ConstExp(10)),
				new CompStmt(new HeapAllocStmt("v", new ConstExp(20)),
						new CompStmt(new HeapAllocStmt("a", new ConstExp(22)), new CompStmt(
								new WriteHeap("a", new ConstExp(30)), new CompStmt(new PrintStmt(new VarExp("a")),
										new PrintStmt(new ReadHeapExp("a")))))));

		PrgState prg7 = new PrgState(new MyStack<>(), new MyDictionary<>(), new MyList<>(), ex7, new MyHeap<>());
		Controller ctrl7 = null;
		try {
			IRepository repo7 = new MyRepository(prg7, ".//logs//log7.txt");
			ctrl7 = new Controller(repo7);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		IStmt ex8 = new CompStmt(new AssignStmt("v", new ConstExp(10)),
				new CompStmt(new HeapAllocStmt("v", new ConstExp(20)),
						new CompStmt(new HeapAllocStmt("a", new ConstExp(22)),
								new CompStmt(new WriteHeap("a", new ConstExp(30)),
										new CompStmt(
												new CompStmt(new PrintStmt(new VarExp("a")),
														new PrintStmt(new ReadHeapExp("a"))),
												new AssignStmt("a", new ConstExp(0)))))));

		PrgState prg8 = new PrgState(new MyStack<>(), new MyDictionary<>(), new MyList<>(), ex8, new MyHeap<>());
		Controller ctrl8 = null;
		try {
			IRepository repo8 = new MyRepository(prg8, ".//logs//log8.txt");
			ctrl8 = new Controller(repo8);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		IStmt ex9 = new CompStmt(new AssignStmt("v", new ConstExp(6)),
				new CompStmt(new WhileStmt(new ArithExp("-", new VarExp("v"), new ConstExp(4)),
						new CompStmt(new CompStmt(new PrintStmt(new ConstExp(80085)), new PrintStmt(new VarExp("v"))),
								new AssignStmt("v", new ArithExp("-", new VarExp("v"), new ConstExp(1))))),
				new PrintStmt(new VarExp("v"))));

		PrgState prg9 = new PrgState(new MyStack<>(), new MyDictionary<>(), new MyList<>(), ex9, new MyHeap<>());
		Controller ctrl9 = null;
		try {
			IRepository repo9 = new MyRepository(prg9, ".//logs//log9.txt");
			ctrl9 = new Controller(repo9);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		IStmt ex10 = new CompStmt(new AssignStmt("v", new ConstExp(10)), 
						new CompStmt(new HeapAllocStmt("a", new ConstExp(22)),
							new CompStmt(new ForkStmt(new CompStmt(new WriteHeap("a", new ConstExp(30)),
								new CompStmt( new AssignStmt("v", new ConstExp(32)), 
										new CompStmt( new PrintStmt(new VarExp("v")), new PrintStmt(new ReadHeapExp("a")))))),
									new CompStmt(new PrintStmt(new VarExp("v")), new PrintStmt(new ReadHeapExp("a"))))));

		PrgState prg10 = new PrgState(new MyStack<>(), new MyDictionary<>(), new MyList<>(), ex10, new MyHeap<>());
		Controller ctrl10 = null;
		try {
			IRepository repo10 = new MyRepository(prg10, ".//logs//log10.txt");
			ctrl10 = new Controller(repo10);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		TextMenu menu = new TextMenu();
		menu.addCommand(new ExitCommand("0", "exit"));
		menu.addCommand(new RunCommand("1", ex1.toString(), ctrl1));
		menu.addCommand(new RunCommand("2", ex2.toString(), ctrl2));
		menu.addCommand(new RunCommand("3", ex3.toString(), ctrl3));
		menu.addCommand(new RunCommand("4", ex4.toString(), ctrl4));
		menu.addCommand(new RunCommand("5", ex5.toString(), ctrl5));
		menu.addCommand(new RunCommand("6", ex6.toString(), ctrl6));
		menu.addCommand(new RunCommand("7", ex7.toString(), ctrl7));
		menu.addCommand(new RunCommand("8", ex8.toString(), ctrl8));
		menu.addCommand(new RunCommand("9", ex9.toString(), ctrl9));
		menu.addCommand(new RunCommand("10", ex10.toString(), ctrl10));
		menu.addCommand(new SerializationCommand("11", "Serialize log1.txt", ctrl1));
		menu.addCommand(new DeserializationCommand("12", "Deserialize log1.txt", ctrl1));

		menu.show();
	}

}
