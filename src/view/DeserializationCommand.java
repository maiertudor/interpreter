package view;

import java.io.IOException;

import controller.Controller;

public class DeserializationCommand extends Command {
	private Controller ctrl;

	public DeserializationCommand(String key, String description, Controller ctrl) {
		super(key, description);
		this.ctrl = ctrl;
	}

	@Override
	public void execute() {
		try {
			ctrl.deserialize();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}

	}

}
