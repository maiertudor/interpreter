package view.GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;

public class InterpreterGUI extends Application{


	public static void main(String[] args) {
		
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		TabPane mainPrg = (TabPane) FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
		Scene scene = new Scene(mainPrg);

		primaryStage.setScene(scene);
		primaryStage.setTitle("Interpreter");

		primaryStage.show();
	}
}
