package view.GUI;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

import controller.Controller;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.PrgState;
import model.ADT.FileData;
import model.ADT.MyDictionary;
import model.ADT.MyHeap;
import model.ADT.MyIDictionary;
import model.ADT.MyIHeap;
import model.ADT.MyList;
import model.ADT.MyStack;
import model.expressions.ArithExp;
import model.expressions.ConstExp;
import model.expressions.ReadHeapExp;
import model.expressions.VarExp;
import model.statements.AssignStmt;
import model.statements.CloseFile;
import model.statements.CompStmt;
import model.statements.ForkStmt;
import model.statements.HeapAllocStmt;
import model.statements.IStmt;
import model.statements.IfStmt;
import model.statements.OpenFile;
import model.statements.PrintStmt;
import model.statements.ReadFile;
import model.statements.WhileStmt;
import model.statements.WriteHeap;
import repository.IRepository;
import repository.MyRepository;

public class InterpreterGUIController implements Initializable {

	private static final ObservableList<IStmt> data = FXCollections.observableArrayList();
	private ObservableList<TabelModel> heapTableData = FXCollections.observableArrayList();
	private ObservableList<String> outputData = FXCollections.observableArrayList();
	private ObservableList<TabelModel> fileTableData = FXCollections.observableArrayList();
	private ObservableList<String> prgIdTableData = FXCollections.observableArrayList();
	private ObservableList<TabelModel> symTableData = FXCollections.observableArrayList();
	private HashMap<Integer, ObservableList<TabelModel>> symDataMappedToProgramStates = new HashMap<>();
	private HashMap<Integer, ObservableList<String>> exeDataMappedToProgramStates = new HashMap<>();

	private IStmt selectedProgram;
	private PrgState selectedProgramState;
	private Controller ctrl;
	private IRepository repository;

	@FXML
	private Label nrPrgLabel;
	@FXML
	private ListView<IStmt> prgStateListView;
	@FXML
	private TableView<TabelModel> heapTableView;
	@FXML
	private TableView<TabelModel> symTableView;
	@FXML
	private TableView<TabelModel> fileTableView;
	@FXML
	private ListView<String> outListView;
	@FXML
	private ListView<String> prgIdListView;
	@FXML
	private ListView<String> exeStackListView;
	@FXML
	private TableColumn<TabelModel, String> varNameColumn;
	@FXML
	private TableColumn<TabelModel, String> varValueColumn;
	@FXML
	private TableColumn<TabelModel, String> fileId;
	@FXML
	private TableColumn<TabelModel, String> fileName;
	@FXML
	private TableColumn<TabelModel, String> heapAddress;
	@FXML
	private TableColumn<TabelModel, String> heapValue;

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		initializeData();

		prgStateListView.setItems(data);
	}

	@FXML
	private void runProgram(ActionEvent event) {
		clearAllData();

		int index = prgStateListView.getSelectionModel().getSelectedIndex();
		selectedProgram = data.get(index);

		PrgState currentPrgState = initializeProgramState(index);

		prgIdTableData.add(currentPrgState.getIdProgram().toString());
		prgIdListView.setItems(prgIdTableData);

		selectedProgramState = currentPrgState;

		storeExeDataToMap();
		Integer idProgram = selectedProgramState.getIdProgram();
		ObservableList<String> exeData = exeDataMappedToProgramStates.get(idProgram);
		exeStackListView.setItems(exeData);
	}

	private PrgState initializeProgramState(int index) {
		PrgState currentPrgState = new PrgState(new MyStack<>(), new MyDictionary<>(), new MyList<>(), selectedProgram,
				new MyDictionary<>(), new MyHeap<>());
		ctrl = null;
		repository = null;
		try {
			repository = new MyRepository(currentPrgState, ".//logs//log" + index + ".txt");
			ctrl = new Controller(repository);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return currentPrgState;
	}

	private void clearAllData() {
		prgIdTableData.clear();
		outputData.clear();
		symTableData.clear();
		fileTableData.clear();
		heapTableData.clear();
		exeDataMappedToProgramStates = new HashMap<>();
		symDataMappedToProgramStates = new HashMap<>();
	}

	@FXML
	private void runStep(ActionEvent event) {

		if (checkProgramTermination()) {
			return;
		}

		outputData.clear();
		symTableData.clear();
		fileTableData.clear();
		heapTableData.clear();

		try {
			ctrl.allStepGUI();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		if (!selectedProgramState.getHeapTable().getContent().isEmpty()) {
			populateHeapTable();
		}

		if (!selectedProgramState.getFileTable().getDict().isEmpty()) {
			populateFileTable();
		}

		populatePrgIdTable();

		populateExeStackTable();

		populateOutputTable();

		populateSymTable();
	}

	@FXML
	private void selectProgram() {
		String selectedProgramId = prgIdListView.getSelectionModel().getSelectedItem();

		selectedProgramState = searchProgram(Integer.parseInt(selectedProgramId));
		// symTableData.clear();
		symTableView.setItems(symDataMappedToProgramStates.get(Integer.parseInt(selectedProgramId)));
		exeStackListView.setItems(exeDataMappedToProgramStates.get(Integer.parseInt(selectedProgramId)));
	}

	private PrgState searchProgram(int selectedProgramId) {
		for (PrgState prg : repository.getPrgList()) {
			if (prg.getIdProgram() == selectedProgramId)
				return prg;
		}
		return null;
	}

	private boolean checkProgramTermination() {
		for (PrgState prg : repository.getPrgList()) {
			if (!prg.getExeStack().isEmpty()) {
				return false;
			}
		}
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Warning");
		alert.setHeaderText("Execution of the program is terminated!");

		alert.showAndWait();
		return true;
	}

	private void populatePrgIdTable() {
		for (PrgState prg : repository.getPrgList()) {
			if (!prgIdTableData.contains(prg.getIdProgram().toString())) {
				prgIdTableData.add(prg.getIdProgram().toString());
			}
		}
		prgIdListView.setItems(prgIdTableData);
	}

	private void populateHeapTable() {
		MyIHeap<Integer, Integer> heapTable = selectedProgramState.getHeapTable();
		for (Integer key : heapTable.getKeys()) {
			TabelModel heapRow = new TabelModel(key.toString(), heapTable.lookUp(key).toString());
			heapTableData.add(heapRow);
		}
		heapAddress.setCellValueFactory(new PropertyValueFactory<TabelModel, String>("key"));
		heapValue.setCellValueFactory(new PropertyValueFactory<TabelModel, String>("value"));
		heapTableView.setItems(heapTableData);
	}

	private void populateFileTable() {
		MyIDictionary<Integer, FileData> fileTable = selectedProgramState.getFileTable();
		for (Integer key : fileTable.getDict().keySet()) {
			TabelModel fileRow = new TabelModel(key.toString(), fileTable.getDict().get(key).toString());
			fileTableData.add(fileRow);
		}
		fileId.setCellValueFactory(new PropertyValueFactory<TabelModel, String>("key"));
		fileName.setCellValueFactory(new PropertyValueFactory<TabelModel, String>("value"));
		fileTableView.setItems(fileTableData);
	}

	private void populateExeStackTable() {
		storeExeDataToMap();

		Integer idProgram = selectedProgramState.getIdProgram();
		ObservableList<String> exeData = exeDataMappedToProgramStates.get(idProgram);
		exeStackListView.setItems(exeData);
	}

	private void storeExeDataToMap() {
		for (PrgState prg : repository.getPrgList()) {
			String exeStack = new String(prg.getExeStack().toString());

			if (!exeDataMappedToProgramStates.containsKey(prg.getIdProgram())) {
				ObservableList<String> clonedExeData = FXCollections.observableArrayList();
				clonedExeData.add(exeStack);
				exeDataMappedToProgramStates.put(prg.getIdProgram(), clonedExeData);
			} else {
				exeDataMappedToProgramStates.get(prg.getIdProgram()).add(exeStack);
			}
		}
	}

	private void populateOutputTable() {
		outputData.add(selectedProgramState.getOut().toString());
		outListView.setItems(outputData);
	}

	private void populateSymTable() {
		addToSymData();
		varNameColumn.setCellValueFactory(new PropertyValueFactory<TabelModel, String>("key"));
		varValueColumn.setCellValueFactory(new PropertyValueFactory<TabelModel, String>("value"));

		symTableView.setItems(symDataMappedToProgramStates.get(selectedProgramState.getIdProgram()));
	}

	private void addToSymData() {
		for (PrgState prg : repository.getPrgList()) {
			MyIDictionary<String, Integer> symTable = prg.getSymTable();
			for (String key : symTable.getDict().keySet()) {
				TabelModel symRow = new TabelModel(key, symTable.getDict().get(key).toString());
				symTableData.add(symRow);
			}
			storeSymDataToSymMap(prg.getIdProgram());
		}
	}

	private void storeSymDataToSymMap(Integer idProg) {
		ObservableList<TabelModel> savedSymTableData = cloneSymData();
		symDataMappedToProgramStates.put(idProg, savedSymTableData);
	}

	private ObservableList<TabelModel> cloneSymData() {
		ObservableList<TabelModel> savedSymTableData = FXCollections.observableArrayList();
		for (TabelModel copy : symTableData) {
			TabelModel el = new TabelModel(copy.getKey(), copy.getValue());
			savedSymTableData.add(el);
		}
		return savedSymTableData;
	}

	private void initializeData() {
		/*
		 * First Statement Example
		 */
		IStmt ex1 = new CompStmt(new AssignStmt("v", new ConstExp(2)), new PrintStmt(new VarExp("v")));
		/*
		 * Second Statement Example
		 */
		IStmt ex2 = new CompStmt(
				new AssignStmt("a",
						new ArithExp("+", new ConstExp(2), new ArithExp("*", new ConstExp(3), new ConstExp(5)))),
				new CompStmt(new AssignStmt("b", new ArithExp("+", new VarExp("a"), new ConstExp(1))),
						new PrintStmt(new VarExp("b"))));
		/*
		 * Third example
		 */
		IStmt ex3 = new CompStmt(new AssignStmt("a", new ArithExp("-", new ConstExp(2), new ConstExp(2))), new CompStmt(
				new IfStmt(new VarExp("a"), new AssignStmt("v", new ConstExp(2)), new AssignStmt("v", new ConstExp(3))),
				new PrintStmt(new VarExp("v"))));
		/*
		 * Fourth Statement example using file operations
		 */
		IStmt ex4 = new CompStmt(new OpenFile("var_f", "test.in"),
				new CompStmt(new ReadFile(new VarExp("var_f"), "var_c"),
						new CompStmt(
								new IfStmt(new VarExp("var_f"),
										new CompStmt(new ReadFile(new VarExp("var_f"), "var_c"),
												new PrintStmt(new VarExp("var_c"))),
										new PrintStmt(new ConstExp(0))),
								new CloseFile(new VarExp("var_f")))));

		IStmt ex5 = new CompStmt(new AssignStmt("v", new ConstExp(10)),
				new CompStmt(new HeapAllocStmt("v", new ConstExp(20)),
						new CompStmt(new HeapAllocStmt("a", new ConstExp(22)), new PrintStmt(new VarExp("v")))));

		IStmt ex6 = new CompStmt(new AssignStmt("v", new ConstExp(10)),
				new CompStmt(new HeapAllocStmt("v", new ConstExp(20)),
						new CompStmt(new HeapAllocStmt("a", new ConstExp(22)), new PrintStmt(new ReadHeapExp("v")))));

		IStmt ex7 = new CompStmt(new AssignStmt("v", new ConstExp(10)),
				new CompStmt(new HeapAllocStmt("v", new ConstExp(20)),
						new CompStmt(new HeapAllocStmt("a", new ConstExp(22)), new CompStmt(
								new WriteHeap("a", new ConstExp(30)), new CompStmt(new PrintStmt(new VarExp("a")),
										new PrintStmt(new ReadHeapExp("a")))))));

		IStmt ex8 = new CompStmt(new AssignStmt("v", new ConstExp(10)),
				new CompStmt(new HeapAllocStmt("v", new ConstExp(20)),
						new CompStmt(new HeapAllocStmt("a", new ConstExp(22)),
								new CompStmt(new WriteHeap("a", new ConstExp(30)),
										new CompStmt(
												new CompStmt(new PrintStmt(new VarExp("a")),
														new PrintStmt(new ReadHeapExp("a"))),
												new AssignStmt("a", new ConstExp(0)))))));

		IStmt ex9 = new CompStmt(new AssignStmt("v", new ConstExp(6)),
				new CompStmt(
						new WhileStmt(new ArithExp("-", new VarExp("v"), new ConstExp(4)),
								new CompStmt(
										new CompStmt(new PrintStmt(new ConstExp(80085)),
												new PrintStmt(new VarExp("v"))),
										new AssignStmt("v", new ArithExp("-", new VarExp("v"), new ConstExp(1))))),
						new PrintStmt(new VarExp("v"))));

		IStmt ex10 = new CompStmt(new AssignStmt("v", new ConstExp(10)),
				new CompStmt(new HeapAllocStmt("a", new ConstExp(22)),
						new CompStmt(
								new ForkStmt(new CompStmt(new WriteHeap("a", new ConstExp(30)),
										new CompStmt(new AssignStmt("v", new ConstExp(32)),
												new CompStmt(new PrintStmt(new VarExp("v")),
														new PrintStmt(new ReadHeapExp("a")))))),
								new CompStmt(new PrintStmt(new VarExp("v")), new PrintStmt(new ReadHeapExp("a"))))));

		data.add(ex1);
		data.add(ex2);
		data.add(ex3);
		data.add(ex4);
		data.add(ex5);
		data.add(ex6);
		data.add(ex7);
		data.add(ex8);
		data.add(ex9);
		data.add(ex10);
	}

}
