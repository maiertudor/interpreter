package view;

import controller.Controller;

public class RunCommand extends Command {
	private Controller ctrl;

	public RunCommand(String key, String description, Controller ctrl) {
		super(key, description);
		this.ctrl = ctrl;
	}

	@Override
	public void execute() {
		try {
			ctrl.allStep();
		} catch (Exception e) {
			System.out.println(e.getClass() + e.getMessage());
//			e.printStackTrace();
		}
	}
}
