package view;

import java.io.IOException;

import controller.Controller;

public class SerializationCommand extends Command{
	private Controller ctrl;

	public SerializationCommand(String key, String description,Controller ctrl) {
		super(key, description);
		this.ctrl = ctrl;
	}

	@Override
	public void execute() {
		try {
			ctrl.serialize(ctrl.repository.getPrgList().get(0));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
