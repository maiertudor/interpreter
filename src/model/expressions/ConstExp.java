package model.expressions;

import java.io.Serializable;

import model.ADT.MyIDictionary;
import model.ADT.MyIHeap;

public class ConstExp extends Exp implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int number;

	public ConstExp(int number) {
		super();
		this.number = number;
	}

	@Override
	public int eval(MyIDictionary<String, Integer> tbl, MyIHeap<Integer, Integer> heap) {
		return number;
	}

	@Override
	public String toString() {
		Integer nr = new Integer(number);
		return nr.toString();
	}

}
