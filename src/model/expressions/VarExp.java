package model.expressions;

import java.io.Serializable;

import model.ADT.MyIDictionary;
import model.ADT.MyIHeap;

public class VarExp extends Exp implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String id;

	public VarExp(String id) {
		super();
		this.id = id;
	}

	@Override
	public int eval(MyIDictionary<String, Integer> tbl, MyIHeap<Integer,Integer> heap) {
		return tbl.lookUp(id);
	}

	@Override
	public String toString() {
		return id;
	}

}
