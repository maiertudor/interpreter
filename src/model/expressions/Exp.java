package model.expressions;

import model.ADT.MyIDictionary;
import model.ADT.MyIHeap;

public abstract class Exp {
	public abstract int eval(MyIDictionary<String, Integer> tbl, MyIHeap<Integer, Integer> heap);

}
