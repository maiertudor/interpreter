package model.expressions;

import java.io.Serializable;

import model.ADT.MyIDictionary;
import model.ADT.MyIHeap;

public class ReadHeapExp extends Exp implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String var_name;

	public ReadHeapExp(String var_name) {
		super();
		this.var_name = var_name;
	}

	@Override
	public int eval(MyIDictionary<String, Integer> tbl, MyIHeap<Integer, Integer> heap) {
		return heap.lookUp(tbl.lookUp(var_name));
	}

	@Override
	public String toString() {
		return "rH(" + var_name + ")";
	}
	
	

}
