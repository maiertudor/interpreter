package model.expressions;

import java.io.Serializable;

import exceptions.InvalidBooleanOperator;
import model.ADT.MyIDictionary;
import model.ADT.MyIHeap;

public class BooleanExp extends Exp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Exp e1;
	Exp e2;
	String op;

	public BooleanExp(Exp e1, String op, Exp e2) throws InvalidBooleanOperator {
		super();
		this.e1 = e1;
		this.e2 = e2;
		if (!"<".equals(op) && !">".equals(op) && !"<=".equals(op) && !">=".equals(op) && !"==".equals(op)
				&& !"!=".equals(op)) {
			throw new InvalidBooleanOperator();
		}
		this.op = op;
	}

	@Override
	public int eval(MyIDictionary<String, Integer> tbl, MyIHeap<Integer, Integer> heap) {
		switch (op) {
		case "<=": {
			if (e1.eval(tbl, heap) <= e2.eval(tbl, heap)) {
				return 1;
			}
			break;
		}
		case ">=": {
			if (e1.eval(tbl, heap) >= e2.eval(tbl, heap)) {
				return 1;
			}
			break;
		}
		case "==": {
			if (e1.eval(tbl, heap) == e2.eval(tbl, heap)) {
				return 1;
			}
			break;
		}
		case "!=": {
			if (e1.eval(tbl, heap) != e2.eval(tbl, heap)) {
				return 1;
			}
			break;
		}
		case ">": {
			if (e1.eval(tbl, heap) > e2.eval(tbl, heap)) {
				return 1;
			}
			break;
		}
		case "<": {
			if (e1.eval(tbl, heap) < e2.eval(tbl, heap)) {
				return 1;
			}
			break;
		}
		}
		return 0;
	}
}
