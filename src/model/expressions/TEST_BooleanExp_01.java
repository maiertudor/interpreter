package model.expressions;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import exceptions.InvalidBooleanOperator;
import model.ADT.MyDictionary;
import model.ADT.MyHeap;
import model.ADT.MyIDictionary;
import model.ADT.MyIHeap;

public class TEST_BooleanExp_01 {

	@Test
	public void test() {
		ArithExp exp = null;
		try {
			exp = new ArithExp("+",new ConstExp(10),new BooleanExp(new ConstExp(2), "<", new ConstExp(6)));
		} catch (InvalidBooleanOperator e) {
			assertEquals(e.getMessage(), "Invalid boolean operator");
		}
		MyIDictionary<String, Integer> tbl = new MyDictionary<>();
		MyIHeap<Integer, Integer> heap = new MyHeap<>();
		int value = exp.eval(tbl, heap);
		assertEquals(value,11);
	}

}
