package model.expressions;

import java.io.Serializable;

import model.ADT.MyIDictionary;
import model.ADT.MyIHeap;

public class ArithExp extends Exp implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Exp e1;
	Exp e2;
	int operation;
	String op;

	public ArithExp(String op, Exp e1, Exp e2) {
		super();
		this.e1 = e1;
		this.e2 = e2;
		if(op.equals("+")){
			this.operation = 1;
		}
		if(op.equals("-")){
			this.operation = 2;
		}
		if(op.equals("*")){
			this.operation = 3;
		}
	}

	@Override
	public int eval(MyIDictionary<String, Integer> tbl, MyIHeap<Integer,Integer> heap) {
		if (operation == 1) {
			return (e1.eval(tbl,heap) + e2.eval(tbl,heap));
		}
		if (operation == 2) {
			return (e1.eval(tbl,heap) - e2.eval(tbl,heap));
		}
		if (operation == 3) {
			return (e1.eval(tbl,heap) * e2.eval(tbl,heap));
		}
		return 0;
	}

	@Override
	public String toString() {
		if (operation == 1) {
			return e1.toString() + "+" + e2.toString();
		}
		if (operation == 2) {
			return e1.toString() + "-" + e2.toString();
		}
		if (operation == 3) {
			return e1.toString() + "*" + e2.toString();
		}
		return "";
	}
}
