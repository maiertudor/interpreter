package model.statements;

import java.io.Serializable;

import model.PrgState;
import model.ADT.MyIStack;

public class CompStmt implements IStmt,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IStmt first;
	private IStmt second;
	
	public CompStmt(IStmt first, IStmt second) {
		super();
		this.first = first;
		this.second = second;
	}

	@Override
	public String toString() {
		return "(" + first.toString() + "; " + second.toString() + ")";
	}

	@Override
	public PrgState execute(PrgState state) {
		MyIStack<IStmt> stk = state.getExeStack();
		stk.push(second);
		stk.push(first);
		return null;
	}
	
}
