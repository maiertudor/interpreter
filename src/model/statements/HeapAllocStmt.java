package model.statements;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;

import exceptions.FileCloseException;
import exceptions.FileOpenedException;
import exceptions.FileReadingException;
import model.PrgState;
import model.ADT.MyIDictionary;
import model.ADT.MyIHeap;
import model.expressions.Exp;

public class HeapAllocStmt implements IStmt,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Integer uniqId = 1;
	private String var_name;
	private Exp expr;

	public HeapAllocStmt(String var_name, Exp expr) {
		super();
		this.var_name = var_name;
		this.expr = expr;
	}

	@Override
	public PrgState execute(PrgState state)
			throws FileOpenedException, FileNotFoundException, IOException, FileCloseException, FileReadingException {
		MyIDictionary<String, Integer> symTable = state.getSymTable();
		MyIHeap<Integer, Integer> heap = state.getHeapTable();
		int evaluation = expr.eval(symTable, heap);
		heap.add(uniqId, evaluation);
		symTable.add(var_name, uniqId);
		uniqId++;
		return null;
	}

	@Override
	public String toString() {
		return "new(" + var_name + ", " + expr.toString() + ")";
	}

}
