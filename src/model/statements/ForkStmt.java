package model.statements;

import java.io.FileNotFoundException;
import java.io.IOException;

import exceptions.FileCloseException;
import exceptions.FileOpenedException;
import exceptions.FileReadingException;
import model.PrgState;
import model.ADT.MyDictionary;
import model.ADT.MyIDictionary;
import model.ADT.MyIStack;
import model.ADT.MyStack;

public class ForkStmt implements IStmt{
	
	IStmt forkStmt;
	
	public ForkStmt(IStmt forkStmt) {
		super();
		this.forkStmt = forkStmt;
	}

	@Override
	public PrgState execute(PrgState state)
			throws FileOpenedException, FileNotFoundException, IOException, FileCloseException, FileReadingException, CloneNotSupportedException {
		
		MyIDictionary<String, Integer> symTable = state.getSymTable();
		MyIDictionary<String, Integer> symTableClone = new MyDictionary<>();
		symTableClone.setDict(symTable.clone());
		
		PrgState newState = new PrgState();
		MyIStack<IStmt> newExeStack = new MyStack<IStmt>();
		newExeStack.push(forkStmt);
		newState.setExeStack(newExeStack);
		newState.setSymTable(symTableClone);
		newState.setHeapTable(state.getHeapTable());
		newState.setFileTable(state.getFileTable());
		newState.setOut(state.getOut());
		newState.newID();
				
		return newState;
	}

	@Override
	public String toString() {
		return "fork(" + forkStmt.toString() + ")";
	}
	

	
}
