package model.statements;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;

import exceptions.FileCloseException;
import exceptions.FileOpenedException;
import exceptions.FileReadingException;
import model.PrgState;
import model.ADT.MyIDictionary;
import model.ADT.MyIHeap;
import model.expressions.Exp;

public class WriteHeap implements IStmt,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String var_name;
	private Exp expr;

	public WriteHeap(String var_name, Exp expr) {
		super();
		this.var_name = var_name;
		this.expr = expr;
	}

	@Override
	public PrgState execute(PrgState state)
			throws FileOpenedException, FileNotFoundException, IOException, FileCloseException, FileReadingException {
		MyIDictionary<String, Integer> symTable = state.getSymTable();
		MyIHeap<Integer, Integer> heapTable = state.getHeapTable();
		int heapKey = symTable.lookUp(var_name);
		int value = expr.eval(symTable, heapTable);
		heapTable.add(heapKey, value);
		return null;
	}

	@Override
	public String toString() {
		return "wH(" + var_name + ", " + expr + ")";
	}
	
	

}
