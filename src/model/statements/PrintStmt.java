package model.statements;

import java.io.Serializable;

import model.PrgState;
import model.ADT.MyIDictionary;
import model.ADT.MyIHeap;
import model.ADT.MyIList;
import model.expressions.Exp;

public class PrintStmt implements IStmt,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Exp exp;

	public PrintStmt(Exp exp) {
		super();
		this.exp = exp;
	}

	@Override
	public String toString() {
		return "print(" + exp.toString() + ")";
	}

	@Override
	public PrgState execute(PrgState state) {
		MyIDictionary<String, Integer> symTbl = state.getSymTable();
		MyIList<Integer> out = state.getOut();
		MyIHeap<Integer, Integer> heap = state.getHeapTable();
		out.add(exp.eval(symTbl,heap));
		return null;
	}

}
