package model.statements;

import java.io.FileNotFoundException;
import java.io.IOException;

import exceptions.FileCloseException;
import exceptions.FileOpenedException;
import exceptions.FileReadingException;
import model.PrgState;

public interface IStmt {
	PrgState execute(PrgState state) throws FileOpenedException, FileNotFoundException, IOException, FileCloseException, FileReadingException, CloneNotSupportedException;
}
