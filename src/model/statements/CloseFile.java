package model.statements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;

import exceptions.FileCloseException;
import model.PrgState;
import model.ADT.FileData;
import model.ADT.MyIDictionary;
import model.ADT.MyIHeap;
import model.expressions.Exp;

public class CloseFile implements IStmt,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Exp var_file_id;
	private String fileName;

	public CloseFile(Exp var_file_id) {
		super();
		this.var_file_id = var_file_id;
	}

	@Override
	public PrgState execute(PrgState state) throws IOException, FileCloseException {
		MyIDictionary<String, Integer> symTable = state.getSymTable();
		MyIDictionary<Integer, FileData> fileTable = state.getFileTable();
		MyIHeap<Integer, Integer> heap = state.getHeapTable();
		Integer uniqId = var_file_id.eval(symTable,heap);
		FileData fileData = fileTable.lookUp(uniqId);
		if (fileData == null) {
			throw new FileCloseException();
		} else {
			BufferedReader reader = fileData.getFileDescriptor();
			reader.close();
			fileTable.delete(uniqId);
		}
		return null;
	}

	@Override
	public String toString() {
		return "CloseFile(" + fileName + ")";
	}

}
