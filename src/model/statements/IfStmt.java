package model.statements;

import java.io.Serializable;

import model.PrgState;
import model.ADT.MyIDictionary;
import model.ADT.MyIHeap;
import model.ADT.MyIStack;
import model.expressions.Exp;

public class IfStmt implements IStmt,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Exp exp;
	public IStmt thenS;
	public IStmt elseS;

	public IfStmt(Exp exp, IStmt thenS, IStmt elseS) {
		super();
		this.exp = exp;
		this.thenS = thenS;
		this.elseS = elseS;
	}

	public String toString(){
		return "IF(" + exp.toString()+")THEN(" + thenS.toString() + ")ELSE(" + elseS.toString() + ")";
	}
	
	public PrgState execute(PrgState state){
		MyIDictionary<String, Integer> symTable = state.getSymTable();
		MyIStack<IStmt> stk = state.getExeStack();
		MyIHeap<Integer, Integer> heap = state.getHeapTable();
		if(exp.eval(symTable,heap) != 0){
			stk.push(thenS);
		}
		else{
			stk.push(elseS);
		}
		return null;
	}

}
