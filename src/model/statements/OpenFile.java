package model.statements;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Serializable;

import exceptions.FileOpenedException;
import model.PrgState;
import model.ADT.FileData;
import model.ADT.MyIDictionary;
import model.ADT.MyIStack;

public class OpenFile implements IStmt,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer uniqId = 0;
	String var_file_id;
	String fileName;

	public OpenFile(String var_file_id, String fileName) {
		super();
		this.var_file_id = var_file_id;
		this.fileName = fileName;
	}

	@Override
	public PrgState execute(PrgState state) throws FileOpenedException, FileNotFoundException {
		MyIStack<IStmt> stk = state.getExeStack();
		MyIDictionary<String, Integer> symTable = state.getSymTable();
		MyIDictionary<Integer, FileData> fileTable = state.getFileTable();
		if (fileTable.getDict().size()!=0) {
			for (FileData el : fileTable.getValues()) {
				if (fileName.equals(el.getFileName())) {
					throw new FileOpenedException();
				}
			}
		}
		else{
//			fileTable = new FileTable<Integer, FileData>();
		}
		uniqId++;
		FileData newEntry = new FileData(fileName, new BufferedReader(new FileReader(fileName)));
		fileTable.add(uniqId, newEntry);
//		state.setFileTable(fileTable);
		symTable.add(var_file_id, uniqId);
		return null;
	}

	@Override
	public String toString() {
		return "OpenFile(" + var_file_id + "," + fileName + ")";
	}

}
