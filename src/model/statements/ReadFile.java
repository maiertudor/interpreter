package model.statements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;

import exceptions.FileReadingException;
import model.PrgState;
import model.ADT.FileData;
import model.ADT.MyIDictionary;
import model.ADT.MyIHeap;
import model.expressions.Exp;

public class ReadFile implements IStmt,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Exp var_file_id;
	String var_name;
	public String fileName;

	public ReadFile(Exp var_file_id, String var_name) {
		super();
		this.var_file_id = var_file_id;
		this.var_name = var_name;
	}

	@Override
	public PrgState execute(PrgState state) throws FileReadingException, IOException {
		MyIDictionary<String, Integer> symTable = state.getSymTable();
		MyIDictionary<Integer, FileData> fileTable = state.getFileTable();
		MyIHeap<Integer, Integer> heap = state.getHeapTable();
		Integer uniqId = var_file_id.eval(symTable,heap);
		FileData fileData = fileTable.lookUp(uniqId);
		if (fileData == null) {
			throw new FileReadingException();
		} else {
			fileName = fileData.getFileName();
			BufferedReader reader = fileData.getFileDescriptor();
			String lineRead = reader.readLine();
			if (lineRead == null) {
				symTable.add(var_name, 0);
			} else {
				symTable.add(var_name,Integer.parseInt(lineRead));
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "ReadFile(" + fileName + ", " + var_name + ")";
	}
	
	

}
