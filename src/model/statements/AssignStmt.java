package model.statements;

import java.io.Serializable;

import model.PrgState;
import model.ADT.MyIDictionary;
import model.ADT.MyIHeap;
import model.expressions.Exp;

public class AssignStmt implements IStmt,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String id;
	Exp exp;

	public AssignStmt(String id, Exp exp) {
		super();
		this.id = id;
		this.exp = exp;
	}

	public String toString() {
		return id + "=" + exp.toString();
	}

	public PrgState execute(PrgState state) {
		MyIDictionary<String, Integer> symTbl = state.getSymTable();
		MyIHeap<Integer, Integer> heap = state.getHeapTable();
		int val = exp.eval(symTbl,heap);
		if (symTbl.isDefined(id)) {
			symTbl.update(id, val);
		} else {
			symTbl.add(id, val);
		}
		return null;
	}
}
