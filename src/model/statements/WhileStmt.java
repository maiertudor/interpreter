package model.statements;

import java.io.FileNotFoundException;
import java.io.IOException;

import exceptions.FileCloseException;
import exceptions.FileOpenedException;
import exceptions.FileReadingException;
import model.PrgState;
import model.ADT.MyIDictionary;
import model.ADT.MyIHeap;
import model.ADT.MyIList;
import model.ADT.MyIStack;
import model.ADT.MyStack;
import model.expressions.Exp;

public class WhileStmt implements IStmt {
	Exp expr;
	IStmt stmt;

	public WhileStmt(Exp expr, IStmt stmt) {
		super();
		this.expr = expr;
		this.stmt = stmt;
	}

	@Override
	public PrgState execute(PrgState state)
			throws FileOpenedException, FileNotFoundException, IOException, FileCloseException, FileReadingException, CloneNotSupportedException {
		MyIHeap<Integer, Integer> heap = state.getHeapTable();
		MyIDictionary<String, Integer> symTbl = state.getSymTable();
		MyIList<Integer> out = state.getOut();
		MyIStack<IStmt> backUpstk = new MyStack<>();
		backUpstk.push(stmt);
		PrgState newState = new PrgState(backUpstk, symTbl, out);
		while (expr.eval(symTbl, heap) > 0) {
			if (backUpstk.isEmpty()) {
				backUpstk.push(stmt);
			} else {
				IStmt st = backUpstk.pop();
				st.execute(newState);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "While(" + expr.toString() + ")" + stmt.toString();
	}

}
