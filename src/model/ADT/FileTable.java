package model.ADT;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class FileTable<T1, T2> implements MyIDictionary<T1, T2> {
	public HashMap<T1, T2> fileTable;

	public FileTable() {
		super();
		this.fileTable = new HashMap<T1, T2>();
	}

	@Override
	public void add(T1 id, T2 val) {
		fileTable.put(id, val);
	}

	@Override
	public void update(T1 id, T2 val) {
		fileTable.put(id, val);
	}

	@Override
	public boolean isDefined(T1 id) {
		return fileTable.containsKey(id);
	}

	@Override
	public T2 lookUp(T1 id) {
		return fileTable.get(id);
	}

	@Override
	public String toString() {
		String output = "";
		for(T1 el : fileTable.keySet()){
			output += el.toString() + "->" + fileTable.get(el).toString() + ", ";
		}
		return output;
	}

	@Override
	public Collection<T2> getValues() {
		return fileTable.values();
	}

	@Override
	public void delete(T1 id) {
		fileTable.remove(id);

	}

	@Override
	public Map<Integer, Integer> getContent() {
		return (Map<Integer, Integer>) fileTable;
	}

	@Override
	public HashMap<T1, T2> clone() {
		return null;
	}

	@Override
	public void setDict(HashMap<T1, T2> dict) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public HashMap<T1, T2> getDict() {
		// TODO Auto-generated method stub
		return null;
	}	
}
