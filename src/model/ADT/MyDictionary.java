package model.ADT;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class MyDictionary<T1, T2> implements MyIDictionary<T1, T2>,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public HashMap<T1, T2> dict;

	public MyDictionary() {
		super();
		this.dict = new HashMap<T1, T2>();
	}
	
	

	public HashMap<T1, T2> getDict() {
		return dict;
	}



	public void setDict(HashMap<T1, T2> dict) {
		this.dict = dict;
	}



	@Override
	public void add(T1 id, T2 val) {
		dict.put(id, val);
	}

	@Override
	public void update(T1 id, T2 val) {
		dict.put(id, val);
	}

	@Override
	public boolean isDefined(T1 id) {
		return dict.containsKey(id);
	}

	@Override
	public T2 lookUp(T1 id) {
		return dict.get(id);
	}

	@Override
	public String toString() {
		return "" + dict + "";
	}

	@Override
	public Collection<T2> getValues() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(T1 id) {
		dict.remove(id);
	}

	@Override
	public Map<Integer, Integer> getContent() {
		return (Map<Integer, Integer>) dict;
	}

	@Override
	public HashMap<T1, T2> clone() {
		HashMap<T1, T2> copy = new HashMap<T1, T2>();
		for (Map.Entry<T1, T2> entry : dict.entrySet()) {
			copy.put(entry.getKey(), entry.getValue());
		}
		return copy;
	}

}
