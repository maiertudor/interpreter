package model.ADT;

import java.util.Map;
import java.util.Set;

public interface MyIHeap<T1,T2>{
	public void add(T1 id,T2 val);
	public T2 lookUp(T1 id);
	public Set<T1> getKeys();
	public void delete(T1 id);
	public Map<Integer, Integer> getContent();
	public void setContent(Map<Integer, Integer> conservativeGarbageCollector);
}
