package model.ADT;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public interface MyIDictionary<T1,T2> {
	public void add(T1 id,T2 val);
	public void update(T1 id,T2 val);
	public boolean isDefined(T1 id);
	public T2 lookUp(T1 id);
	public Collection<T2> getValues();	
	public void delete(T1 id);
	public Map<Integer, Integer> getContent();
	public HashMap<T1, T2> clone();
	public void setDict(HashMap<T1, T2> dict);
	public HashMap<T1, T2> getDict();
}
