package model.ADT;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MyList<T> implements MyIList<T>,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public List<T> list;

	public MyList() {
		super();
		this.list = new ArrayList<T>();
	}

	@Override
	public void add(T el) {
		list.add(el);
	}

	@Override
	public int size() {
		return list.size();
	}

	@Override
	public String toString() {
		return "" + list + "";
	}

}
