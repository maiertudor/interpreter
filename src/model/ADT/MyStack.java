package model.ADT;

import java.io.Serializable;
import java.util.Stack;

public class MyStack<T> implements MyIStack<T>,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Stack<T> stack;

	public MyStack() {
		super();
		this.stack = new Stack<T>();
	}

	@Override
	public T pop() {
		return stack.pop();
	}

	@Override
	public void push(T el) {
		stack.push(el);
	}

	@Override
	public boolean isEmpty() {
		return stack.isEmpty();
	}

	@Override
	public String toString() {
		return "" + stack + "";
	}

}
