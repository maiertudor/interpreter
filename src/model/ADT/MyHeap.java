package model.ADT;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MyHeap<T1, T2> implements MyIHeap<T1, T2>,Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public HashMap<T1, T2> heap;

	public MyHeap() {
		heap = new HashMap<>();
	}

	@Override
	public void add(T1 id, T2 val) {
		heap.put(id, val);
	}

	@Override
	public T2 lookUp(T1 id) {
		return heap.get(id);
	}

	@Override
	public Set<T1> getKeys() {
		return heap.keySet();
	}

	@Override
	public void delete(T1 id) {
		heap.remove(id);
	}

	@Override
	public String toString() {
		return "" + heap;
	}

	@Override
	public Map<Integer, Integer> getContent() {
		return (Map<Integer, Integer>) heap;
	}

	@Override
	public void setContent(Map<Integer, Integer> conservativeGarbageCollector) {
		heap = (HashMap<T1, T2>) conservativeGarbageCollector;
		
	}

}
