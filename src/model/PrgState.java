package model;

import java.io.IOException;
import java.io.Serializable;

import exceptions.FileCloseException;
import exceptions.FileOpenedException;
import exceptions.FileReadingException;
import exceptions.MyStmtExecException;
import model.ADT.FileData;
import model.ADT.MyIDictionary;
import model.ADT.MyIHeap;
import model.ADT.MyIList;
import model.ADT.MyIStack;
import model.statements.IStmt;

public class PrgState implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	MyIStack<IStmt> exeStack;
	MyIDictionary<String, Integer> symTable;
	MyIList<Integer> out;
	IStmt originalProgram;
	MyIDictionary<Integer, FileData> fileTable;
	MyIHeap<Integer, Integer> heapTable;
	private Integer idProgram = 1;

	public PrgState(MyIStack<IStmt> exeStack, MyIDictionary<String, Integer> symTable, MyIList<Integer> out,
			IStmt originalProgram) {
		super();
		this.exeStack = exeStack;
		this.symTable = symTable;
		this.out = out;
		this.originalProgram = originalProgram;
		exeStack.push(originalProgram);
	}

	public PrgState(MyIStack<IStmt> exeStack, MyIDictionary<String, Integer> symTable, MyIList<Integer> out,
			MyIDictionary<Integer, FileData> fileTable) {
		super();
		this.exeStack = exeStack;
		this.symTable = symTable;
		this.out = out;
		this.fileTable = fileTable;
	}

	public PrgState(MyIStack<IStmt> exeStack, MyIDictionary<String, Integer> symTable, MyIList<Integer> out) {
		super();
		this.exeStack = exeStack;
		this.symTable = symTable;
		this.out = out;
	}

	public PrgState(MyIStack<IStmt> exeStack, MyIDictionary<String, Integer> symTable, MyIList<Integer> out,
			IStmt originalProgram, MyIDictionary<Integer, FileData> fileTable, MyIHeap<Integer, Integer> heapTable) {
		super();
		this.exeStack = exeStack;
		this.symTable = symTable;
		this.out = out;
		this.originalProgram = originalProgram;
		this.fileTable = fileTable;
		this.heapTable = heapTable;
		exeStack.push(originalProgram);
	}

	public PrgState(MyIStack<IStmt> exeStack, MyIDictionary<String, Integer> symTable, MyIList<Integer> out,
			IStmt originalProgram, MyIHeap<Integer,Integer> heapTable) {
		super();
		this.exeStack = exeStack;
		this.symTable = symTable;
		this.out = out;
		this.originalProgram = originalProgram;
		this.heapTable = heapTable;
		exeStack.push(originalProgram);
	}

	public PrgState() {
	}

	public MyIStack<IStmt> getExeStack() {
		return exeStack;
	}

	public void setExeStack(MyIStack<IStmt> exeStack) {
		this.exeStack = exeStack;
	}

	public MyIDictionary<String, Integer> getSymTable() {
		return symTable;
	}

	public void setSymTable(MyIDictionary<String, Integer> symTable) {
		this.symTable = symTable;
	}

	public MyIList<Integer> getOut() {
		return out;
	}

	public void setOut(MyIList<Integer> out) {
		this.out = out;
	}

	public IStmt getOriginalProgram() {
		return originalProgram;
	}

	public void setOriginalProgram(IStmt originalProgram) {
		this.originalProgram = originalProgram;
	}

	public MyIDictionary<Integer, FileData> getFileTable() {
		return fileTable;
	}

	public void setFileTable(MyIDictionary<Integer, FileData> fileTable) {
		this.fileTable = fileTable;
	}

	public MyIHeap<Integer, Integer> getHeapTable() {
		return heapTable;
	}

	public void setHeapTable(MyIHeap<Integer, Integer> heapTable) {
		this.heapTable = heapTable;
	}

	public String toStr() {
		String result = "ID: " + idProgram + "\n" + "ExeStack:\t" + exeStack.toString() + "\n" + "SymTable:\t" + symTable.toString() + "\n"
				+ "Output:\t\t" + out.toString();
		if (fileTable != null) {
			result += "\nFileTable:\t" + fileTable.toString() + "\n"; 
		} else if (heapTable != null){
			result += "\nHeapTable:\t" + heapTable.toString() + "\n";
		}
		return result;
	}

	public Boolean isNotCompleted() {
		return !exeStack.isEmpty();
	}

	public PrgState oneStep()
			throws FileOpenedException, MyStmtExecException, IOException, FileCloseException, FileReadingException, CloneNotSupportedException {

		if (exeStack.isEmpty()) {
			throw new MyStmtExecException();
		}
		
		IStmt currentStmt = exeStack.pop();
		return currentStmt.execute(this);
	}
	
	public void newID(){
		this.idProgram++;
	}

	public Integer getIdProgram() {
		return idProgram;
	}

	public void setIdProgram(Integer idProgram) {
		this.idProgram = idProgram;
	}

}
