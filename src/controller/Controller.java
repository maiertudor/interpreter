package controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import model.PrgState;
import repository.IRepository;

public class Controller {
	public IRepository repository;
	public ExecutorService executor;

	public Map<Integer, Integer> conservativeGarbageCollector(Collection<Integer> symTableValues,
			Map<Integer, Integer> heap) {

		return (Map<Integer, Integer>) heap.entrySet().stream().filter(e -> symTableValues.contains(e.getKey()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	public Controller(IRepository repo) {
		repository = repo;
	}


	public void allStep() throws Exception {
		executor = Executors.newFixedThreadPool(2);
		while (true) {
			List<PrgState> prgList = removeCompletedPrg(repository.getPrgList());
			if (prgList.size() == 0) {
				break;
			}
			oneStepForAllPrg(prgList);
		}
		repository.closeFile();
		executor.shutdownNow();
	}

	public void allStepGUI() throws InterruptedException {
		executor = Executors.newFixedThreadPool(2);
		// remove the completed programs
		List<PrgState> prgList = removeCompletedPrg(repository.getPrgList());
		if (prgList.size() == 0) {
			// display a window message saying that the execution terminates
			executor.shutdownNow();
		} else {
			oneStepForAllPrg(prgList);
			executor.shutdownNow();
		}
	}

	public void oneStepForAllPrg(List<PrgState> prgList) throws InterruptedException {

		prgList.forEach(prg -> {
			try {
				repository.logPrgStateExec(prg);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		});

		List<Callable<PrgState>> callList = prgList.stream().map(p -> (Callable<PrgState>) (() -> {
			return p.oneStep();
		})).collect(Collectors.toList());

		List<PrgState> newPrgList = executor.invokeAll(callList).stream().map(future -> {
			try {
				return future.get();
			} catch (Exception e) {
				e.printStackTrace();
				;
				return null;
			}
		}).filter(p -> p != null).collect(Collectors.toList());

		prgList.addAll(newPrgList);

		prgList.forEach(prg -> {
			try {
				repository.logPrgStateExec(prg);
			} catch (IOException e) {
				e.printStackTrace();
			}
		});

		repository.setPrgList(prgList);
	}

	public void serialize(PrgState prgState) throws IOException {
		repository.serializeFile(prgState);
	}

	public void deserialize() throws FileNotFoundException, ClassNotFoundException, IOException {
		repository.deserializeFile();
	}

	public List<PrgState> removeCompletedPrg(List<PrgState> prgList) {

		List<PrgState> result = prgList.stream().filter(p -> p.isNotCompleted()).collect(Collectors.toList());

		return result;
	}
}

// public void allStep() throws Exception {
// PrgState program = repository.getCurrentPrg();
// try {
// while (true) {
// PrgState currentStep = oneStep(program);
//
// // Garbage collector
// if (currentStep.getHeapTable() != null) {
// currentStep.getHeapTable().setContent(conservativeGarbageCollector(
// currentStep.getSymTable().getContent().values(),
// currentStep.getHeapTable().getContent()));
// }
// repository.logPrgStateExec();
// }
// } catch (MyStmtExecException e) {
// System.out.println(e.getMessage());
// }
// repository.closeFile();
// }